<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('department');
            $table->integer('ans1')->nullable();
            $table->integer('ans2')->nullable();
            $table->integer('ans3')->nullable();
            $table->integer('ans4')->nullable();
            $table->integer('ans5')->nullable();
            $table->integer('ans6')->nullable();
            $table->integer('ans7')->nullable();
            $table->integer('ans8')->nullable();
            $table->integer('ans9')->nullable();
            $table->integer('ans10')->nullable();
            $table->integer('ans11')->nullable();
            $table->integer('ans12')->nullable();
            $table->integer('ans13')->nullable();
            $table->integer('ans14')->nullable();
            $table->integer('ans15')->nullable();
            $table->integer('ans16')->nullable();
            $table->integer('ans17')->nullable();
            $table->integer('ans18')->nullable();
            $table->integer('ans19')->nullable();
            $table->integer('ans20')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
