<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            // 'name' => 'Hardik',
            // 'email' => 'admin@gmail.com',
            // 'password' => bcrypt('123456'),

            'name' => 'admin',
            'email' => 'it@lannaagro.com',
            'username' => 'admin',
            // 'email_verified_at' => now(),
            'password' => bcrypt('123456'), // password
            'remember_token' => Str::random(10),
            
        ]);
    }
}
