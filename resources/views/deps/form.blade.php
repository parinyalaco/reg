<div class="form-group col-md-6 {{ $errors->has('devision') ? 'has-error' : ''}}">
    <label for="devision" class="control-label">{{ 'ฝ่าย' }}</label>
    <input class="form-control" name="devision" type="text" id="devision" value="{{ $dep->devision or ''}}" >
    {!! $errors->first('devision', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-6 {{ $errors->has('department') ? 'has-error' : ''}}">
    <label for="department" class="control-label">{{ 'แผนก' }}</label>
    <input class="form-control" name="department" type="text" id="department" value="{{ $dep->department or ''}}" >
    {!! $errors->first('department', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
