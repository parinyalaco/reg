@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Reg {{ $reg->id }}</div>
                    <div class="card-body">
                        
                        <div class="col-md-8">
                            <a href="{{ url('/regs') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ url('/regs/' . $reg->id . '/edit') }}" title="Edit Reg"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            
                            <form method="POST" action="{{ url('regs' . '/' . $reg->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Reg" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            </form>
                            <a href="{{ url('/regs/clear_reg/' . $reg->id) }}" title="Export Comment"><button class="btn btn-success btn-sm"> ล้างการลงทะเบียน (reg {{ $staff_reg }} คน,ans {{ $staff_ans }} คน)</button></a>
                        </div>
                        <div class="col-md-4">
                            <a href="{{ url('/regs/upload/' . $reg->id) }}" title="Upload Quiz"><button class="btn btn-success btn-sm"> Upload Quiz</button></a>
                            <a href="{{ url('/regs/export/' . $reg->id) }}" title="Export Comment"><button class="btn btn-success btn-sm"> Export Comment</button></a>
                            
                        </div>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $reg->id }}</td>
                                    
                                        <th>Name</th><td>{{ $reg->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Desc</th><td>{{ $reg->desc }}</td>
                                    
                                        <th>start</th><td>{{ $reg->start_reg }}</td>
                                    </tr>
                                    <tr>
                                        <th>End</th><td>{{ $reg->end_reg }}</td>
                                    
                                        <th>Staff</th><td>{{ $reg->staff->count() }}</td>
                                    </tr>
                                </tbody>
                            </table>
                             <table class="table">
                                <thead>
                                    <tr>
                                        <th>รหัส</th>
                                        <th>ชื่อ-สุกล</th>
                                        <th>ผ่าย/แผนก</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($staffs as $item)
                                        <tr>
                                            <td>{{ $item->code }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->dep->devision }} / {{ $item->dep->department }}</td>
                                        </tr>
                                    @endforeach 
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $staffs->appends(['search' => Request::get('search')])->render() !!} </div>
                       
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
