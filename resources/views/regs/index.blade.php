@extends('layouts.app')
<style>
    .switch {
      position: relative;
      display: inline-block;
      width: 30px;
      height: 20px;
    }
    
    .switch input { 
      opacity: 0;
      width: 0;
      height: 0;
    }
    
    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }
    
    .slider:before {
      position: absolute;
      content: "";
      height: 13px;
      width: 13px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }
    
    input:checked + .slider {
      background-color: #2196F3;
    }
    
    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }
    
    input:checked + .slider:before {
      -webkit-transform: translateX(13px);
      -ms-transform: translateX(13px);
      transform: translateX(13px);
    }
    
    /* Rounded sliders */
    .slider.round {
      border-radius: 20px;
    }
    
    .slider.round:before {
      border-radius: 50%;
    }
</style>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Regs</div>
                    <div class="card-body">
                        <a href="{{ url('/regs/create') }}" class="btn btn-success btn-sm" title="Add New Reg">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>                        

                        <form method="GET" action="{{ url('/regs') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Start - End</th>
                                        <th>ระบบ</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($regs as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ date('Y-m-d H:i',strtotime($item->start_reg)) }} - {{ date('Y-m-d H:i',strtotime($item->end_reg)) }}</td>
                                        <td>                                            
                                            @if($item->active=='Active')
                                                {{-- <a href="{{ url('/qr_code/' . $item->id) }}" title="QR Code" target="_blank"><button class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> QR Code</button></a> --}}
                                                <a href="{{ url('/regs/change_active/1/' . $item->id) }}" title="ปิด"><button class="btn btn-primary btn-sm"> ปิด</button></a>
                                                {{-- <div class="row my-4"> --}}
                                                    <label class="switch" title="Delete Reg" ><input type="checkbox" @if($item->set_start=='1') checked @endif><span class="slider round"></span></label>
                                                    {{-- <label for="set_start" class="control-label col">ทำประเมิน</label>  --}}
                                                    <input type="hidden" id="reg_id" value="{{ $item->id }}">                                           
                                                {{-- </div> --}}
                                            @else
                                                <a href="{{ url('/regs/change_active/0/' . $item->id) }}" title="เปิด"><button class="btn btn-primary btn-sm"> เปิด</button></a>
                                            @endif

                                        </td>
                                        <td>
                                            <a href="{{ url('/logs/listDiv/' . $item->id ) }}" title="Register" target="_blank"><button class="btn btn-primary btn-sm"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i> ลงทะเบียน</button></a>

                                            <a href="{{ url('/logs/showAll/' . $item->id ) }}" title="Report" target="_blank"><button class="btn btn-primary btn-sm"><i class="fa fa-pie-chart" aria-hidden="true"></i> ผลสรุป</button></a>
                                            {{-- <a href="{{ url('/imports/upload/' . $item->id) }}" title="Upload Staff"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Upload</button></a> --}}
                                            <a href="{{ url('/staffs/index/' . $item->id) }}" title="Staff Data"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>พนักงาน</button></a> 

                                            <a href="{{ url('/regs/' . $item->id) }}" title="View Reg"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/regs/' . $item->id . '/edit') }}" title="Edit Reg"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</button></a>
                                            
                                            <form method="POST" action="{{ url('/regs' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Reg" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>                                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $regs->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/set_start.js')}}"></script>

@endsection
