<div class="row">
<div class="form-group col-md-4 {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $reg->name or ''}}" required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group col-md-4 {{ $errors->has('start_reg') ? 'has-error' : ''}}">
        <label for="start_reg" class="control-label">{{ 'วัน-เวลา' }}</label>
        <input class="form-control" name="start_reg" type="datetime-local" id="start_reg" 
        value = "@php
            if(isset($reg->start_reg)){
                echo date('Y-m-d\TH:i',strtotime($reg->start_reg));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
        {!! $errors->first('start_reg', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-md-4 {{ $errors->has('end_reg') ? 'has-error' : ''}}">
        <label for="end_reg" class="control-label">{{ 'วัน-เวลา' }}</label>
        <input class="form-control" name="end_reg" type="datetime-local" id="end_reg" 
        value = "@php
            if(isset($reg->end_reg)){
                echo date('Y-m-d\TH:i',strtotime($reg->end_reg));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
        {!! $errors->first('end_reg', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-md-12 {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'Desc' }}</label>
    <input class="form-control" name="desc" type="text" id="desc" value="{{ $reg->desc or ''}}" required>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
</div>