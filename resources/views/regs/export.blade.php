<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    tr th {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    tr th.noborder {
        border: none;
        word-wrap: normal;
    }
     tr th.noborder-last {
        border: none;
        word-wrap: normal;
    }
    tr th.noborderr {
        border: none;
        text-align: right;
        word-wrap: break-word;
    }
    tr th.noborderc {
        border: none;
        text-align: center;
        word-wrap: break-word;
        font: bolder;
    }
    tr td {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">ความพึงพอใจ</th>
                <th colspan="4">จำนวนคน</th>
            </tr>
            <tr>
                <th>หัวข้อหลัก</th>
                <th>หัวข้อย่อย</th>

                <th>ดีมาก</th>
                <th>ดี</th>
                <th>ปานกลาง</th>
                <th>ควรปรับปรุง</th>
            </tr>
        </thead>
        <tbody>
            @php 
                $q=1; 
                $sum = array();
            @endphp
            @foreach($query_quiz as $item)   
                <tr>             
                    <td>@if(empty($desc) || ($desc<>$item->desc)){{ $item->desc }}@endif</td>
                    @php $desc = $item->desc; @endphp
                    <td>{{ $item->ans_map }}</td>
                    @for($i=4; $i>=1; $i--)
                        <td>@if(!empty($ans[$q][$i])){{ $ans[$q][$i] }}@endif</td>
                        @php
                            if(!empty($ans[$q][$i])){
                                if(empty($sum[$i])){
                                    $sum[$i] = $ans[$q][$i];
                                }else{
                                    $sum[$i] += $ans[$q][$i];
                                }
                            }
                        @endphp
                        {{-- <td></td> --}}
                    @endfor
                </tr> 
                @php $q++; @endphp              
            @endforeach
            <tr>             
                <td colspan="2">สรุปจำนวน</td>
                @for($i=1; $i<=4; $i++)
                    {{-- <td></td> --}}
                    <td>@if(!empty($sum[$i])){{ $sum[$i] }}@endif</td>
                @endfor
            </tr>
        </tbody>
    </table>
</body>
</html>