@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Staff {{ $staff->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/staffs/index/'.$staff->reg_id) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/staffs/' . $staff->id . '/edit') }}" title="Edit Staff"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('staffs' . '/' . $staff->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Staff" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $staff->id }}</td>
                                        <th>Code</th><td>{{ $staff->code }}</td>
                                        <th>Name</th><td>{{ $staff->name }}</td>
                                        <th>Devision</th><td>{{ $staff->dep->devision }}</td>
                                        <th>Department</th><td>{{ $staff->dep->department }}</td>
                                        <th>Status</th><td>{{ $staff->status }}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                            @if(!empty($quiz[0]))
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>หัวข้อ</th>
                                            <th>ความพึงพอใจ</th>
                                            <th>ระดับความคิดเห็น</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=0; @endphp
                                        @foreach ($quiz as $item)
                                            <tr>
                                                {{-- <td>@if(empty($quiz_name) || ($quiz_name<>$item->name)){{ $item->name }}@endif</td>
                                                @php $quiz_name = $item->name; @endphp --}}
                                                <td>@if(empty($quiz_desc) || ($quiz_desc<>$item->desc)){{ $item->desc }}@endif</td>
                                                @php $quiz_desc = $item->desc; @endphp
                                                <td>{{ $item->ans_map }}</td>
                                                <td>@if(!empty($ans[$i])){{ $ans[$i] }}@endif</td>
                                            </tr>
                                            @php $i++; @endphp
                                        @endforeach
                                        <tr>
                                            <td>ความคิดเห็นเพิ่มเติม</td>
                                            <td colspan="2">@if(!empty($ans['note'])){{ $ans['note'] }}@endif</td>
                                    </tbody>
                                </table>
                            @endif
                        </div>

                        

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
