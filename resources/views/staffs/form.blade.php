<div class="row">
<div class="form-group col-md-3 {{ $errors->has('code') ? 'has-error' : ''}}">
    <label for="code" class="control-label">{{ 'Code' }}</label>
    <input name="reg_id" type="hidden" id="reg_id" value="{{ $staff->reg_id}}" >
    <input class="form-control" name="code" type="text" id="code" value="{{ $staff->code or ''}}" >
    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('dep_id') ? 'has-error' : ''}}">
        {!! Form::label('dep_id', 'เขต', ['class' => 'control-label']) !!}
        @if (isset($staff->dep_id))
            {!! Form::select('dep_id', $departmentlist,$staff->dep_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('dep_id', $departmentlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('dep_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-3 {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $staff->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
        @if (isset($staff->status))
            {!! Form::select('status', $statuslist,$staff->status, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('status', $statuslist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12 form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
</div>