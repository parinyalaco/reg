<div class="row">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Name</label>
    
        <div class="col-md-6">
            <input id="name" type="text" class="form-control" name="name" value="{{ $reg->name or '' }}" required autofocus>
    
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
    
        <div class="col-md-6">
            <input id="email" type="email" class="form-control" name="email" value="{{ $reg->email or '' }}" required>
    
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
        <label for="username" class="col-md-4 control-label">User Name</label>
    
        <div class="col-md-6">
            <input id="username" type="text" class="form-control" name="username" value="{{ $reg->username or '' }}" required autofocus>
    
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-4 control-label">Password</label>
    
        <div class="col-md-6">
            <input id="password" type="password" class="form-control" name="password">
    
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="form-group">
        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
    
        <div class="col-md-6">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
        </div>
    </div>

    <div class="col-md-12 form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-6">
            <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
        </div>
        
    </div>
</div>
