@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>{{ $check_txt['name'] }}</h3></div>
                    <div class="card-body">
                        <h2>{{ $check_txt['txt'] }}</h2>
                        @if($check_txt['chk']==0)
                            <a href="{{ url('quizs/add/'.$check_txt['id']) }}" ><button type="button" class="btn btn-success btn-lg">หน้าหลักการประเมิน</button></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection