@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>{{ $quiz[0]['name'] }}</h3></div>
                    <div class="card-body">
                        
                            <form method="POST" action="{{ url('quizs/addAction/'.$id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" >
                           {{ csrf_field() }}

                                <div class="row">
                            {{-- <div class="col-md-12">
                                <label for="">แผนก</label>
                                <input type="text" class="form-control" name="department" placeholder="แผนก"  id="department"  value="{{ request('department') }}" required>
                            </div> --}}
                            <div class="col-md-12">
                                <table class="table">
                                <thead>
                                    <tr>
                                        <th width="80%">ความพึงพอใจ</th>
                                        <th>ระดับความคิดเห็น</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=0 @endphp
                                    @foreach ( $quiz as $key)
                                        @if(empty($desc) || ($desc <> $key->desc))
                                            <tr>
                                                <td colspan="2"><b>{{ $key->desc }}</b></td>
                                            </tr>
                                        @endif
                                        @php $desc = $key->desc; @endphp                                       
                                        <tr>
                                            <td>{{ $key->ans_map }}</td>
                                            <td>
                                                <select name="{{ 'ans'.++$i }}" id="{{ 'ans'.$i }}" required>
                                                    <option value="">-</option>
                                                    <option value="4">ดีมาก</option>
                                                    <option value="3">ดี</option>
                                                    <option value="2">ปานกลาง</option>
                                                    <option value="1">ควรปรับปรุง</option>
                                                </select>
                                            </td>
                                        </tr>
                                        
                                    @endforeach                                    
                                    <tr>
                                        <td colspan="2"> <label for="">ความคิดเห็นเพิ่มเติม/ข้อเสนอแนะ</label><br/>
                                        <textarea width='100%' class="form-control" id="note"  name="note" placeholder="note" ></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                            <div class="col-md-12"><span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                         บันทึก
                                    </button>
                                </span></div>
                                </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection