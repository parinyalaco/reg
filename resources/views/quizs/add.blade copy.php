@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>แบบประเมินความพึงพอใจงานรับมอบนโยบายประจำปี 2563</h3></div>
                    <div class="card-body">
                        
                            <form method="POST" action="{{ url('quizs/addAction/'.$id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" >
                           {{ csrf_field() }}

                                <div class="row">
                            {{-- <div class="col-md-12">
                                <label for="">แผนก</label>
                                <input type="text" class="form-control" name="department" placeholder="แผนก"  id="department"  value="{{ request('department') }}" required>
                            </div> --}}
                            <div class="col-md-12">
                                <table class="table">
                                <thead>
                                    <tr>
                                        <th width="80%">ความพึงพอใจ</th>
                                        <th>ระดับความคิดเห็น</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2"><b>1. เนื้อหาที่ได้รับมีประโยชน์,น่าสนใจ</b></td>
                                    </tr>
                                    <tr>
                                        <td>1.1 สามารถเข้าใจ เนื้อหาสาระ ที่นำเสนอ ตรงประเด็น</td>
                                        <td>
                                            <select name="ans1" id="ans1"  required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1.2 เข้าใจเหตุการณ์และปัญหา ที่เกิดขึ้นในปี 2562</td>
                                        <td>
                                            <select name="ans2" id="ans2" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1.3 เข้าใจแนวทางการทำงานขององค์กรในปี 2563</td>
                                        <td>
                                            <select name="ans3" id="ans3" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1.4 เข้าใจแนวทางการทำงานของตนเองในปี 2563</td>
                                        <td>
                                            <select name="ans4" id="ans4" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><b>2.ผู้นำเสนอสื่อสารเข้าใจง่าย,ชัดเจน</b></td>
                                    </tr>
                                    <tr>
                                        <td>2.1 วิทยากรบรรยาย ชัดเจนและเข้าใจง่าย</td>
                                        <td>
                                            <select name="ans5" id="ans5" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2.2 ใช้เทคนิคที่ใช้ในการนำเสนอ ชัดเจน ไม่สับสน</td>
                                        <td>
                                            <select name="ans6" id="ans6" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><b>3. ความพร้อมของสถานที่ เครื่องเสียง จอภาพ</b></td>
                                    </tr>
                                    <tr>
                                        <td>3.1 ความพึงพอใจเกี่ยวกับสถานที่</td>
                                        <td>
                                            <select name="ans7" id="ans7" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3.2 เครื่องเสียง จอภาพ มีความชัดเจน</td>
                                        <td>
                                            <select name="ans8" id="ans8" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3.3 ข้อมูลที่นำเสนอ สามารถ อ่านได้ มองเห็นชัดเจน</td>
                                        <td>
                                            <select name="ans9" id="ans9" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><b>4.  อาหารว่าง,เครื่องดื่ม</b></td>
                                    </tr>
                                    <tr>
                                        <td>4.1 อาหารและเครื่องดื่ม มีความเหมาะสม</td>
                                        <td>
                                            <select name="ans10" id="ans10" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4.2 ความพึงพอใจของ น้ำฟักทองและน้ำมันม่วง</td>
                                        <td>
                                            <select name="ans11" id="ans11" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><b>5. ความพึงพอใจโดยรวมในการจัดงานรับมอบนโยบายประจำปี 2563</b></td>
                                    </tr>
                                    <tr>
                                        <td>5.1 การได้รับข้อมูลก่อนการสัมมนา</td>
                                        <td>
                                            <select name="ans12" id="ans12" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5.2 ความยาก ง่าย ในการลงทะเบียน</td>
                                        <td>
                                            <select name="ans13" id="ans13" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5.3 มีความพึงพอใจที่ได้เข้าร่วมกิจกรรม</td>
                                        <td>
                                            <select name="ans14" id="ans14" required>
                                                <option value="">-</option>
                                                <option value="4">ดีมาก</option>
                                                <option value="3">ดี</option>
                                                <option value="2">ปานกลาง</option>
                                                <option value="1">ควรปรับปรุง</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> <label for="">ความคิดเห็นเพิ่มเติม/ข้อเสนอแนะ</label><br/>
                                        <textarea width='100%' class="form-control" id="note"  name="note" placeholder="note" ></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                            <div class="col-md-12"><span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                         บันทึก
                                    </button>
                                </span></div>
                                </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection