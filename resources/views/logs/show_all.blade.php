@extends('layouts.graph')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>{{ $reg->name }} 
                <a href="{{ url('/logs/showAll/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-success btn-lg">สรุปรวม</button></a>
                <a href="{{ url('/logs/showDiv/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปฝ่าย</button></a>
                <a href="{{ url('/logs/showDep/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปแผนก</button></a>                
                {{-- <a href="{{ url('/logs/showAns/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ระดับความพึงพอใจ</button></a> --}}
                <a href="{{ url('/logs/listregistered/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้มาลงทะเบียน</button></a>
                <a href="{{ url('/logs/listunregistered/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้ยังไม่มาลงทะเบียน</button></a>
                 <a href="{{ url('/logs/listregisteredtxt/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้มาลงทะเบียนสุ่ม</button></a>
                </h3></div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($summary as $item)
                               <div class='col-md-12'>
                               <div id="chart_div_{{$item->id}}"  style=" height: 500px;">Loading</div>   
                                </div> 
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    @foreach ($summary as $item)
      google.charts.setOnLoadCallback(drawChart_{{$item->id}});

      function drawChart_{{$item->id}}() {

        var data = google.visualization.arrayToDataTable([
          ['Type', 'จำนวนคน'],
          ['ลงทะเบียนแล้ว',     {{$item->regstaff}}],
          ['ยังไม่ได้ลงทะเบียน',      {{$item->unregstaff}}],
        ]);

        var options = {
          title: 'สรุป{{$item->name}}'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_{{$item->id}}'));

        chart.draw(data, options);
      }
      @endforeach
    </script>
@endsection
