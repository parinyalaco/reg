@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>{{ $reg->name }}</h3></div>
                    <div class="card-body">
                        <div class="row">
                            <form method="GET" action="{{ url('logs/listDiv/'.$reg->id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                           
                            <div class="col-md-3"><input type="text" class="form-control" name="search" placeholder="รหัสพนักงาน 8 หลัก 0100XXXX" value="{{ request('search') }}">
                               </div>
                            <div class="col-md-3"><span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i> ค้นหา
                                    </button>
                                </span></div>
                        </form>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>รหัส</th>
                                        <th>ชื่อ-สุกล</th>
                                        <th>ผ่าย/แผนก</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (empty(request('search')))
                                        <tr>
                                            <td colspan="4">พิมพ์ข้อมูลด้านบน และกดค้นหา</td>
                                        </tr>
                                    @else
                                        @if (sizeof($staffUnRegist) == 0)
                                            <tr>
                                            {{-- <td colspan="4">ไม่พบรหัสนี้ หรือ รหัสนี้ลงทะเบียนไปแล้ว</td> --}}
                                            <td colspan="4">ไม่พบข้อมูล</td>
                                        </tr>
                                        @else
                                            @foreach ($staffUnRegist as $item)
                                        <tr>
                                            <td>{{ $item->code }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->devision }} / {{ $item->department }}</td>
                                            <td>
                                                @if($item->regstaff==0 || $item->makequiz==0)
                                                    @if ($item->regstaff==0)
                                                        <a href="{{ url('/logs/register/'.$reg->id.'/'.$item->id.'/'.request('search')) }}" title="ลงทะเบียน"><button type="button" class="btn btn-info btn-lg">ลงทะเบียน</button></a>
                                                    @else
                                                    @if ($item->makequiz==0 && $item->set_start==1 && (!empty($quiz[0])))
                                                        <a href="{{ url('/quizs/add/'.$item->id) }}" title="ลงทะเบียน"><button type="button" class="btn btn-info btn-lg">ทำแบบประเมิน</button></a>
                                                            {{-- <a href="{{ url('/quizs/add/'.$item->id) }}" title="ลงทะเบียน"><button type="button" class="btn btn-info btn-lg" @if($item->set_start==0) disabled @endif>ทำแบบประเมิน</button></a> --}}
                                                    @endif
                                                    
                                                    @endif
                                                @else
                                                    พนักงานได้ทำการลงทะเบียน และ ทำแบบประเมิน แล้ว
                                                @endif
                                            </td>
                                        </tr>
                                         @endforeach
                                        @endif
                                        
                                    @endif
                                     
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
