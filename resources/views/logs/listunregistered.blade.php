@extends('layouts.table')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>{{ $reg->name }} 
                <a href="{{ url('/logs/showAll/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปรวม</button></a>
                <a href="{{ url('/logs/showDiv/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปฝ่าย</button></a>
                <a href="{{ url('/logs/showDep/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปแผนก</button></a>
                <a href="{{ url('/logs/listregistered/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้มาลงทะเบียน</button></a>
                <a href="{{ url('/logs/listunregistered/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-success btn-lg">ผู้ยังไม่มาลงทะเบียน</button></a>
                 <a href="{{ url('/logs/listregisteredtxt/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้มาลงทะเบียนสุ่ม</button></a>
                <br/>
                จำนวนที่ยังไม่ได้ลงทะเบียน : {{$staffUnRegist->total()}}
                </h3></div>
                <div class="card-body">
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>รหัส</th>
                                        <th>ชื่อ-สุกล</th>
                                        <th>ผ่าย/แผนก</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($staffUnRegist as $item)
                                        <tr>
                                            <td>{{ $item->code }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->devision }} / {{ $item->department }}</td>
                                            <td>
                                                <a href="{{ url('/logs/register/'.$reg->id.'/'.$item->id) }}" title="ลงทะเบียน"><button type="button" class="btn btn-info btn-lg">ลงทะเบียน</button></a>
                                            </td>
                                        </tr>
                                    @endforeach 
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $staffUnRegist->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
