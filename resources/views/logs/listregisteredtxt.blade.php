@extends('layouts.table')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>{{ $reg->name }} 
                <a href="{{ url('/logs/showAll/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปรวม</button></a>
                <a href="{{ url('/logs/showDiv/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปฝ่าย</button></a>
                <a href="{{ url('/logs/showDep/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปแผนก</button></a>
                <a href="{{ url('/logs/listregistered/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้มาลงทะเบียน</button></a>
                <a href="{{ url('/logs/listunregistered/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้ยังไม่มาลงทะเบียน</button></a>
                <a href="{{ url('/logs/listregisteredtxt/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-success btn-lg">ผู้มาลงทะเบียนสุ่ม</button></a>
                <br/>
                จำนวนที่ลงทะเบียน : {{$registerStafff->count()}}   
                <a href="https://www.abcya.com/games/random_name_picker" target="_blank" >App สุ่ม</a>
                </h3></div>
                    <div class="card-body">
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>รหัส/ชื่อ-สุกล</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($registerStafff as $item)
                                        <tr><td>{{ $item->staff->name }} / {{ $item->staff->dep->department }}</td></tr>
                                    @endforeach 
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
