@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>
                    {{ $reg->name }} ฝ่าย {{ $dep->devision }} แผนก {{ $dep->department }} <br/>
                    <a href="{{ url('/logs/listDep/'.$reg->id.'/'.$dep->devision) }}" title="Back"><button type="button" class="btn btn-info btn-lg">Back</button></a>
                       </h3></div>
                    <div class="card-body">
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>รหัส</th>
                                        <th>ชื่อ-สุกล</th>
                                        <th>ผ่าย/แผนก</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($staffUnRegist as $item)
                                        <tr>
                                            <td>{{ $item->code }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->devision }} / {{ $item->department }}</td>
                                            <td>
                                                <a href="{{ url('/logs/register/'.$reg->id.'/'.$item->id) }}" title="ลงทะเบียน"><button type="button" class="btn btn-info btn-lg">ลงทะเบียน</button></a>
                                            </td>
                                        </tr>
                                    @endforeach 
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $staffUnRegist->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
