@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>
                    {{ $reg->name }} ฝ่าย {{ $devision }} <br/>
                    <a href="{{ url('/logs/listDiv/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">Back</button></a>
                       </h3></div>
                    <div class="card-body">
                        <div class="row">
                            <form method="GET" action="{{ url('logs/listDep/'.$reg->id.'/'.$devision) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                           
                            <div class="col-md-4"><input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                               </div>
                            <div class="col-md-4"><span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span></div>
                        </form>
                        </div>
                        <div class="row">
                            @foreach ($depList as $item)
                            <div class="col-md-4">
                                <a href="{{ url('/logs/liststaff/'.$reg->id.'/'.$item->id) }}" title="Back"><button type="button" class="btn btn-primary btn-lg btn-block">{{$item->department}}</button></a>
                            </div>  
                            @endforeach
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>รหัส</th>
                                        <th>ชื่อ-สุกล</th>
                                        <th>ผ่าย/แผนก</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($staffUnRegist as $item)
                                        <tr>
                                            <td>{{ $item->code }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->devision }} / {{ $item->department }}</td>
                                            <td>
                                                <a href="{{ url('/logs/register/'.$reg->id.'/'.$item->id) }}" title="ลงทะเบียน"><button type="button" class="btn btn-info btn-lg">ลงทะเบียน</button></a>
                                            </td>
                                        </tr>
                                    @endforeach 
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $staffUnRegist->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
