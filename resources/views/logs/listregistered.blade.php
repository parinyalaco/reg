@extends('layouts.table')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header"><h3>{{ $reg->name }} 
                <a href="{{ url('/logs/showAll/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปรวม</button></a>
                <a href="{{ url('/logs/showDiv/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปฝ่าย</button></a>
                <a href="{{ url('/logs/showDep/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">สรุปแผนก</button></a>
                <a href="{{ url('/logs/listregistered/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-success btn-lg">ผู้มาลงทะเบียน</button></a>
                <a href="{{ url('/logs/listunregistered/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้ยังไม่มาลงทะเบียน</button></a>
                 <a href="{{ url('/logs/listregisteredtxt/'.$reg->id) }}" title="Back"><button type="button" class="btn btn-info btn-lg">ผู้มาลงทะเบียนสุ่ม</button></a>
                <br/>
                จำนวนที่ลงทะเบียน : {{$registerStafff->total()}}   
                </h3></div>
                    <div class="card-body">
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>วันเวลา</th>
                                        <th>รหัส/ชื่อ-สุกล</th>
                                        <th>ผ่าย/แผนก</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($registerStafff as $item)
                                        <tr>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->staff->code }}/{{ $item->staff->name }}</td>
                                            <td>{{ $item->staff->dep->devision }} / {{ $item->staff->dep->department }}</td>
                                            <td>
                                                <a href="{{ url('/logs/unregister/'.$reg->id.'/'.$item->id) }}" title="ยกเลิก"><button type="button" class="btn btn-info btn-lg">ยกเลิก</button></a>
                                            </td>
                                        </tr>
                                    @endforeach 
                                </tbody>
                            </table>
                            
                            <div class="pagination-wrapper"> {!! $registerStafff->appends(['search' => Request::get('search')])->render() !!} </div>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
