<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'department', 'ans1', 'ans2', 'ans3', 'ans4',
        'ans5', 'ans6', 'ans7', 'ans8', 'ans9', 'ans10',
        'ans11', 'ans12', 'ans13', 'ans14', 'ans15', 'ans16',
        'ans17', 'ans18', 'ans19', 'ans20', 'note','staff_id'
    ];
}
