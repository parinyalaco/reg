<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'reg_id'
      ,'staff_id'
      ,'ref'];

  public function reg()
  {
    return $this->hasOne('App\Reg', 'id', 'reg_id');
  }

  public function staff()
  {
    return $this->hasOne('App\Staff', 'id', 'staff_id');
  }
}
