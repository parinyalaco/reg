<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = [
        'reg_id', 'dep_id', 'code', 'name', 'status'
    ];

    public function reg()
    {
        return $this->hasOne('App\Reg', 'id', 'reg_id');
    }

    public function dep()
    {
        return $this->hasOne('App\Dep', 'id', 'dep_id');
    }
}
