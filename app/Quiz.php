<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable = [
        'name', 'desc', 'ans_map', 'reg_id', 'status'
    ];

    public function reg()
    {
        return $this->hasOne('App\Reg', 'id', 'reg_id');
    }
}
