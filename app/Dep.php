<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dep extends Model
{
    protected $fillable = [
        'devision', 'department'
    ];
}
