<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

use App\Reg;
use App\Staff;
use App\Quiz;
use App\Log;
use App\Answer;
use Illuminate\Http\Request;


// use Carbon\Carbon;
// use DB;
use SimpleXLSX;
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RegsController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $regs = Reg::where(function ($q) use ($keyword) {
                $q->Where('name', 'like', '%' . $keyword . '%');
            })->where('show',1)->latest()->paginate($perPage);
            // $regs = Reg::latest()->paginate($perPage);
        } else {
            $regs = Reg::where('show',1)->latest()->paginate($perPage);
        }
        // dd($regs);

        return view('regs.index', compact('regs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('regs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();

        $str_reg = \Carbon\Carbon::parse($requestData['start_reg'])->format('Y-m-d H:i');
        $end_reg = \Carbon\Carbon::parse($requestData['end_reg'])->format('Y-m-d H:i');
        if($str_reg>$end_reg){
            return redirect()->back()->withErrors(['error' => 'การกำหนดช่วงเวลาผิดพลาด!!']);
        }
        $regs = Reg::whereRaw("'".$str_reg."' between start_reg and end_reg")->orWhereRaw("'".$end_reg."' between start_reg and end_reg")->count();
        // dd($regs);
        if($regs>0){
            return redirect()->back()->withErrors(['error' => 'ช่วงเวลาซ้ำกับที่มีอยู่!!']);
        }

        $requestData['start_reg'] = \Carbon\Carbon::parse($requestData['start_reg'])->format('Y-m-d H:i');
        $requestData['end_reg'] = \Carbon\Carbon::parse($requestData['end_reg'])->format('Y-m-d H:i');
        $requestData['show'] = 1;

        Reg::create($requestData);

        return redirect('regs')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $reg = Reg::findOrFail($id);

        $perPage = 25;
        $staffs = Staff::where('reg_id',$id)->where('status','Active')->paginate($perPage);
        $staff_reg = Log::where('reg_id',$id)->count();
        $staff_ans = Answer::join('staff', 'answers.staff_id', '=', 'staff.id')->where('staff.reg_id', '=', $id)->count();

        return view('regs.show', compact('reg', 'staffs','staff_reg','staff_ans'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $reg = Reg::findOrFail($id);

        return view('regs.edit', compact('reg'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $str_reg = \Carbon\Carbon::parse($requestData['start_reg'])->format('Y-m-d H:i');
        $end_reg = \Carbon\Carbon::parse($requestData['end_reg'])->format('Y-m-d H:i');
        if($str_reg>$end_reg){
            return redirect()->back()->withErrors(['error' => 'การกำหนดช่วงเวลาผิดพลาด!!']);
        }
        $regs = Reg::where('id','<>',$id)->whereRaw("('".$str_reg."' between start_reg and end_reg or '".$end_reg."' between start_reg and end_reg)")->count();
        // dd($regs);
        if($regs>0){
            return redirect()->back()->withErrors(['error' => 'ช่วงเวลาซ้ำกับที่มีอยู่!!']);
        }

        $requestData['start_reg'] = \Carbon\Carbon::parse($requestData['start_reg'])->format('Y-m-d H:i');
        $requestData['end_reg'] = \Carbon\Carbon::parse($requestData['end_reg'])->format('Y-m-d H:i');

        $reg = Reg::findOrFail($id);
        $reg->update($requestData);

        return redirect('regs')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // dd($id);
        $staff = Staff::where('reg_id',$id)->count();
        $quiz = Quiz::where('reg_id',$id)->count();
        $reg = Reg::findOrFail($id);
        
        if($staff>0 || $quiz>0){
            $set_show['show'] = 0;
            $reg->update($set_show);
        }else{
            $reg->destroy($id);
        }        

        return redirect('regs')->with('flash_message', ' deleted!');
    }

    public function change_active($status, $id)
    {
        $reg = Reg::findOrFail($id);
        $set_active['active']='Active';
        $set_active['set_start']=0;
        $set_Dactive['active']='Disable';

        if($status==1){ //ปิด
            $reg->update($set_Dactive);
        }else{            //เปิด
            $reg->update($set_active);

            $regD = Reg::where('id','<>', $id);      
            $regD->update($set_Dactive);
        }
        return redirect('regs')->with('flash_message', ' updated!');
    }

    public function set_start(Request $request)
    {
        $requestData = $request->all();  
        // dd($requestData);      
        $status = $requestData['status'];  
        $id = $requestData['id'];
        $reg = Reg::findOrFail($id);
        if($status=='Checked'){
            $set_active['set_start']=1;
            $txt = 'เปิดให้ทำแบบประเมินแล้วค่ะ';
        }else{
            $set_active['set_start']=0;
            $txt = 'ปิดระบบประเมินแล้วค่ะ';
        }
        $query = $reg->update($set_active);
        // return response()->json($query);
        //  dd($query);

        // return redirect('regs')->with('flash_message', ' updated!');
        return $txt;
    }

    public function upload($reg_id){
        return view('regs.upload',compact('reg_id'));
    }

    public function uploadAction(Request $request,$reg_id){

        if ($request->hasFile('uploadfile')) {

            $filename = "fileName_" . time() . '_' . $request->file('uploadfile')->getClientOriginalName();

            $uploadfilepath = $request->file('uploadfile')->storeAs('public/excel', $filename);

            $realPathFile =  Storage::disk('public')->path('excel');
            $quiz_status['status'] = 'Disable';
            $count_quiz = Quiz::where('reg_id','=',$reg_id)->where('status','=','Active')->update($quiz_status);

            $realfile = $realPathFile . "\\" . $filename;
            // dd($realfile);
            // if ($xlsx = SimpleXLSX::parse($realfile)) {
            //     foreach ($xlsx->rows() as $r => $row) {
            //         if ($r > 0) {
            //             $user_load['name'] = $row[0];
            //             $user_load['desc'] = $row[1];
            //             $user_load['ans_map'] = $row[2];
            //             $user_load['reg_id'] = $reg_id;
            //             $user_load['status'] = 'Active';                          
            //             // dd($product_load);
            //             $query_user = Quiz::create($user_load);                            
            //         }
            //     }
            //     return redirect(route('regs.show',$reg_id))->with('success','Import successfully');
            // }

            Excel::load($realfile, function ($reader) use ( $reg_id ) {
                
                $reader->each(function ($row) use ( $reg_id ) {

                    $tmp = array();
                    // echo $row->depcode."---</br>";
                    $tmp['name'] = trim($row->name);
                    $tmp['desc'] = trim($row->desc);
                    $tmp['ans_map'] = trim($row->ans_map);
                    $tmp['reg_id'] = $reg_id;
                    $tmp['status'] = 'Active';
                    // var_dump($row->depcode);
                    Quiz::create($tmp);
                });
            });
        }
        return redirect(route('regs.show',$reg_id))->with('success','Import successfully');
    }

    public function export($reg_id){

        $data = Staff::join('regs', 'staff.reg_id', '=', 'regs.id')->join('answers', 'staff.id', '=', 'answers.staff_id')
                    ->where('regs.id', $reg_id)->select('regs.name','answers.*')->get();
        // dd($data[0]['name']);
                    
        $query_quiz = Quiz::where('reg_id','=',$reg_id)->where('status','=','Active')->select('*')->get();            

        $ans = array();            
        // $ans['name'] = $query_quiz[0]['name'];
        foreach($data as $key2){
            if(!empty($key2->id)){
                $i=1;
                foreach($query_quiz as $key){                
                    $choose = "ans".$i;
                    // if($key2->$choose == 4){
                        if(empty($ans[$i][$key2->$choose])){
                            $ans[$i][$key2->$choose] = 1;
                        }else{
                            $ans[$i][$key2->$choose] += 1;
                        }
                    $i++;
                }
            }
        }
        // dd($ans);
        // return view('regs.export',compact('ans','query_quiz'))->with('success','Import successfully');
        // return view('regs.edit', compact('reg'));



        $filename = "สรุปแบบสอบถาม_" . date('ymdHi');
        // dd($query_quiz);
        
        Excel::create($filename, function ($excel) use ($ans,$query_quiz) {               
            $excel->sheet('แบบสอบถาม', function ($sheet) use ($ans,$query_quiz) {                    
                $sheet->loadView('regs.export')->with('ans', $ans)->with('query_quiz', $query_quiz);
            });
        })->export('xlsx');    

    }

    public function clear_reg($reg_id){
        $staff_ans = Answer::join('staff', 'answers.staff_id', '=', 'staff.id')->where('staff.reg_id', '=', $reg_id)->delete();
        $staff_reg = Log::where('reg_id',$reg_id)->delete();
        

        return redirect(route('regs.show',$reg_id))->with('flash_message','Clear successfully');
    }
}
