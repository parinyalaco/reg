<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Answer;
use App\Dep;
use App\Staff;

class QuizsController extends Controller
{
    public function add($id){ 
        // SELECT quizzes.*
        // FROM   quizzes INNER JOIN
        //      staff ON quizzes.reg_id = staff.reg_id
        // WHERE (staff.id = 1850) 
        $quiz = Quiz::join('staff', 'quizzes.reg_id', '=', 'staff.reg_id')->where('staff.id', $id)->where('quizzes.status', 'Active')->select('quizzes.*')->get();   
        // dd($quiz[0]['name']);
        return view('quizs.add',compact('id','quiz'));
    }

    public function addAction(Request $request,$id){
        $check_ans = "";
        $check_txt = array();        
        $reg = new Staff;
        $reg =$reg->join('regs', 'staff.reg_id', '=', 'regs.id')->where('staff.id', '=', $id)->select('regs.name', 'regs.set_start', 'regs.active','staff.reg_id')->get();
        
        $check_ans = Answer::where('staff_id', '=', $id)->count();
        if($check_ans==0){
            if($reg[0]['active']=='Active' && $reg[0]['set_start']==1 && $check_ans==0){
                
                $requestData = $request->all();

                $requestData['staff_id'] = $id;
                $requestData['reg_id'] = $reg[0]['reg_id'];
                $check_ans = Answer::create($requestData);  
                
                $check_txt['chk'] = 1;
                $check_txt['txt'] = 'ขอบคุณที่ร่วมทำการประเมิน';
            }else{
                $check_txt['chk'] = 0;
                $check_txt['txt'] = 'ขอโทษค่ะ ระบบเกิดข้อผิดพลาด กรุณาทำแบบประเมินใหม่อีกครั้ง';
            }
        }
        else{
            $check_txt['chk'] = 1;
            $check_txt['txt'] = 'ระบบเคยได้รับแบบประเมินจากคุณแล้ว ไม่สามารถบันทึกค่าใหม่ได้';
        }        
        $check_txt['id'] = $id;
        $check_txt['name'] = $reg[0]['name'];
        // dd($check_txt);
        // return redirect('quizs/success',compact('check_txt',));         
        return view('quizs.success',compact('check_txt'));
    }

    // public function success($check_txt){
    //     return view('quizs.success',compact('check_txt'));
    // }

    public function add_quiz($id){ 
        $quiz = Quiz::where('reg_id', $id)->where('status', 'Active')->select('*')->get();   
        $dep = Dep::orderBy('department')->select('id','department')->get();
        
        return view('quizs.add',compact('id','quiz','dep'));
    }
}
