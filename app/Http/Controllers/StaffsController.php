<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

use App\Staff;
use App\Dep;
use App\Answer;
use App\Quiz;
use Illuminate\Http\Request;

class StaffsController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, $id)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $staffs = Staff::where(function ($q) use ($keyword) {
                    $q->where('staff.code', 'like', '%' . $keyword . '%')
                        ->orWhere('staff.name', 'like', '%' . $keyword . '%');
                })->where('reg_id', $id)->where('status', 'Active')->orderBy('id')->paginate($perPage);
        } else {
            $staffs = Staff::where('reg_id',$id)->where('status', 'Active')->orderBy('id')->paginate($perPage);
        }
        return view('staffs.index', compact('staffs','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $statuslist = array('Active' => 'Active', 'Disable' => 'Disable');
        $departmentlist = Dep::pluck('department','id');
        return view('staffs.create',compact('departmentlist', 'statuslist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        

        Staff::create($requestData);

        return redirect('staffs')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $staff = Staff::findOrFail($id);
        $quiz = Quiz::join('staff', 'quizzes.reg_id', '=', 'staff.reg_id')->where('staff.id',$id)->where('quizzes.status', 'Active')->select('*')->get();
        $answer = Answer::where('staff_id', $id)->select('*')->get();
        // dd(count($answer));
        $ans = array();
        if(count($answer)>0){
            for($i=0; $i<20; $i++){
                $ans_no = 'ans'.($i+1); 
                if(!empty($answer[0][$ans_no])){
                    if($answer[0][$ans_no]==4){
                        $choose = 'ดีมาก';
                    }elseif($answer[0][$ans_no]==3){
                        $choose = 'ดี';
                    }elseif($answer[0][$ans_no]==2){
                        $choose = 'ปานกลาง';
                    }elseif($answer[0][$ans_no]==1){
                        $choose = 'ควรปรับปรุง';
                    }else{
                        $choose = '-';
                    } 
                    $ans[] = $choose;
                }                    
                
                if(!empty($answer[0][$ans_no]))     $ans['note'] = $answer[0]['note'];    
            }
        }
        
        // dd($ans);
        return view('staffs.show', compact('staff','quiz','answer','ans'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $statuslist = array('Active'=>'Active','Disable'=>'Disable');
        $staff = Staff::findOrFail($id);
        $departmentlist = Dep::pluck('department', 'id');

        return view('staffs.edit', compact('staff', 'departmentlist', 'statuslist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // echo "update";
        $requestData = $request->all();
        // dd($requestData);
        $reg_id = $requestData['reg_id'];
        // dd($reg_id);
        $staff = Staff::findOrFail($id);
        // dd($requestData);
        $staff->update($requestData);

        return redirect(route('staffs_index',$reg_id))->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $staff = Staff::findOrFail($id);
        // dd($staff->reg_id);
        Staff::destroy($id);

        return redirect(route('staffs_index',$staff->reg_id))->with('flash_message', ' deleted!');
    }

    public function upload($reg_id){
        return view('staffs.upload',compact('reg_id'));
    }

    public function uploadAction(Request $request,$reg_id){
        if ($request->hasFile('uploadfile')) {
            // var_dump($requestData['uploadfile']);

            $filename = "fileName_" . time() . '_' . $request->file('uploadfile')->getClientOriginalName();

            //echo $filename;

            $uploadfilepath = $request->file('uploadfile')->storeAs('public/excel', $filename);

            // echo $uploadfilepath;

            $realPathFile =  Storage::disk('public')->path('excel');

            
            $alldeprw = Dep::get();
            $alldep = array();
            foreach ($alldeprw as $value) {
                $alldep[trim($value->department)] = $value->id;
            }
            // dd($alldep);
            $staff_status['status'] = 'Disable';
            $count_staff = Staff::where('reg_id','=',$reg_id)->where('status','=','Active')->update($staff_status);

            // $count_staff = Staff::where('reg_id','=',$reg_id)->where('status','=','Active')->get();
            // // dd($count_staff);
            // if(!empty($count_staff)){
            //     $staff_status['status'] = 'Disable';
            //     $count_staff->update($staff_status);
            // }

            $realfile = $realPathFile . "\\" . $filename;
            Excel::load($realfile, function ($reader) use ( $reg_id , $alldep) {
                
                $reader->each(function ($row) use ( $reg_id , $alldep) {

                    $tmp = array();
                    // echo $row->depcode."---</br>";
                    $tmp['reg_id'] = $reg_id;
                    $tmp['dep_id'] = $alldep[trim($row->depcode)];
                    $tmp['code'] = trim($row->code);
                    $tmp['name'] = trim($row->name);
                    $tmp['status'] = 'Active';
                    // var_dump($row->depcode);
                    Staff::create($tmp);
                });
            });

            return redirect(route('staffs_index',$reg_id))->with('flash_message', ' Import success!!');
        }
    }
}
