<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\Reg;
use App\Dep;
use App\Log;
use App\Quiz;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

use Illuminate\Support\Facades\DB;

class LogsController extends Controller
{
    public function listDiv($reg_id, Request $request){

        $reg = Reg::findOrFail($reg_id);
        $quiz = Quiz::where('reg_id',$reg_id)->where('status', 'Active')->select('*')->get();
        // dd(empty($quiz[0]));
        $depList = DB::table('staff')
            ->join('deps', 'deps.id', '=', 'staff.dep_id')

            ->select('deps.devision')
            ->where('staff.reg_id', $reg_id)
            ->where('staff.status','Active')
            ->groupBy('deps.devision')
            ->get();

        $keyword = $request->get('search');
        $perPage = 25;
        // dd($keyword);
        $staffUnRegist =array();

        if (!empty($keyword)) {
            $staffUnRegist = DB::table('staff')
                ->leftjoin('logs', function ($join) {
                    $join->on('staff.id', '=', 'logs.staff_id');
                    $join->on('staff.reg_id', '=', 'logs.reg_id');
                })
                ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
                ->leftjoin('regs', 'staff.reg_id', '=', 'regs.id')
                ->leftjoin('answers', 'staff.id', '=', 'answers.staff_id')
                // ->select('staff.id', 'staff.code', 'staff.name', 'deps.devision', 'deps.department')
                // ->where('logs.id')
                ->select(DB::raw('
                    staff.id, 
                    staff.code, 
                    staff.name, 
                    deps.devision, 
                    deps.department,
                    count(logs.staff_id) as regstaff,
                    count(answers.staff_id) as makequiz,
                    regs.set_start
                '))
                ->where('staff.reg_id', $reg_id)
                ->where('staff.code',$keyword)
                ->where('staff.status', 'Active')  
                ->where('regs.active', 'Active')               
                ->groupBy('staff.id', 'staff.code', 'staff.name', 'deps.devision', 'deps.department','regs.set_start')
                ->orderBy('staff.name')
                ->paginate($perPage);
        }else{
           /* $staffUnRegist = DB::table('staff')
                ->leftjoin('logs', function ($join) {
                    $join->on('staff.id', '=', 'logs.staff_id');
                    $join->on('staff.reg_id', '=', 'logs.reg_id');
                })
                ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
                ->select('staff.id', 'staff.code', 'staff.name', 'deps.devision', 'deps.department')
                ->where('logs.id')
                ->where('staff.reg_id', $reg_id)
                ->orderBy('staff.code')
                ->paginate($perPage);
                
            */
        }

        // dd($staffUnRegist);

        return view('logs.list_div',compact('depList', 'reg', 'staffUnRegist','quiz'));
    }

    public function listDep($reg_id, $devision, Request $request)
    {
        $this->middleware('isAdmin');
        $reg = Reg::findOrFail($reg_id);

        $depList = DB::table('staff')
            ->join('deps', 'deps.id', '=', 'staff.dep_id')
            ->select('deps.id','deps.department')
            ->where('staff.reg_id', $reg_id)
            ->where('deps.devision', $devision)
            ->where('staff.status', 'Active')
            ->groupBy('deps.id', 'deps.department')
            ->get();

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $staffUnRegist = DB::table('staff')
                ->leftjoin('logs', function ($join) {
                    $join->on('staff.id', '=', 'logs.staff_id');
                    $join->on('staff.reg_id', '=', 'logs.reg_id');
                })
                ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
                ->select('staff.id', 'staff.code', 'staff.name', 'deps.devision', 'deps.department')
                ->where('logs.id')
                ->where('staff.reg_id', $reg_id)
                ->where('deps.devision', $devision)
                ->where(function ($q) use ($keyword) {
                    $q->where('staff.code', 'like', '%' . $keyword . '%')
                        ->orWhere('staff.name', 'like', '%' . $keyword . '%');
                })
                ->orderBy('staff.code')
                ->paginate($perPage);
        }else{
            $staffUnRegist = DB::table('staff')
                ->leftjoin('logs', function ($join) {
                    $join->on('staff.id', '=', 'logs.staff_id');
                    $join->on('staff.reg_id', '=', 'logs.reg_id');
                })
                ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
                ->select('staff.id', 'staff.code', 'staff.name', 'deps.devision', 'deps.department')
                ->where('logs.id')
                ->where('staff.reg_id', $reg_id)
                ->where('deps.devision', $devision)
                ->orderBy('staff.code')
                ->paginate($perPage);  
        }

        return view('logs.list_dep', compact('depList', 'reg','devision', 'staffUnRegist'));
    }

    public function liststaff($reg_id, $dep_id)
    {
        $this->middleware('isAdmin');
        $reg = Reg::findOrFail($reg_id);
        $dep = Dep::findOrFail($dep_id);

        $perPage = 25;

        $staffUnRegist = DB::table('staff')
            ->leftjoin('logs', function ($join) {
                $join->on('staff.id', '=', 'logs.staff_id');
                $join->on('staff.reg_id', '=', 'logs.reg_id');
            })
            ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
            ->select('staff.id', 'staff.code', 'staff.name', 'deps.devision', 'deps.department')
            ->where('logs.id')
            ->where('staff.reg_id', $reg_id)
            ->where('deps.id', $dep_id)
            ->orderBy('staff.code')
            ->paginate($perPage);

        return view('logs.liststaff', compact( 'reg', 'dep', 'staffUnRegist'));
    }

    public function register($reg_id,$staff_id,$search,Request $request){
        $this->middleware('isAdmin');
        $chk = Log::where('reg_id',$reg_id)->where('staff_id',$staff_id)->first();
        $user_agent = $request->header('User-Agent');
        if(empty($chk)){
            $tmp = array();
            $tmp['reg_id'] = $reg_id;
            $tmp['staff_id'] = $staff_id;
            $tmp['ref'] = $user_agent;
            var_dump($tmp);
            Log::create($tmp);
        }

        return redirect('logs/listDiv/'. $reg_id.'?search='.$search);
    }

    public function unregister($reg_id,$id){
        $this->middleware('isAdmin');
        Log::destroy($id);

        return redirect('logs/listregistered/'. $reg_id  )->with('flash_message', ' deleted!');
    }

    public function listregistered($reg_id){
        $this->middleware('isAdmin');
        $reg = Reg::findOrFail($reg_id);
        $perPage = 25;
        $registerStafff = Log::where('reg_id',$reg_id)->orderBy('created_at','desc')->paginate($perPage);

        return view('logs.listregistered', compact('reg', 'registerStafff'));
    }

    public function listregisteredtxt($reg_id)
    {
        $this->middleware('isAdmin');
        $reg = Reg::findOrFail($reg_id);
        $registerStafff = Log::where('reg_id', $reg_id)->orderBy('created_at', 'desc')->get();

        return view('logs.listregisteredtxt', compact('reg', 'registerStafff'));
    }

    public function showDiv($reg_id){
        
        $this->middleware('isAdmin');        

        $reg = Reg::findOrFail($reg_id);

        $summary = DB::table('staff')
            ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
            ->leftjoin('logs', function ($join) {
                $join->on('staff.id', '=', 'logs.staff_id');
                $join->on('staff.reg_id', '=', 'logs.reg_id');
            })
            ->select(DB::raw('
            deps.devision,
            count(staff.id) as allstaff,
            count(logs.staff_id) as regstaff,
            count(staff.id) - count(logs.staff_id) as unregstaff
            '))
            ->where('staff.reg_id',$reg_id)
            ->where('staff.status', 'Active')
            ->groupBy('deps.devision')
            ->orderBy('deps.devision')
            ->get();

        return view('logs.show_div', compact('reg', 'summary'));
    }

    public function showDep($reg_id)
    {
        $this->middleware('isAdmin');
        $reg = Reg::findOrFail($reg_id);

        $summary = DB::table('staff')
            ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
            ->leftjoin('logs', function ($join) {
                $join->on('staff.id', '=', 'logs.staff_id');
                $join->on('staff.reg_id', '=', 'logs.reg_id');
            })
            ->select(DB::raw('
            deps.id,
            deps.devision,
            deps.department,
            count(staff.id) as allstaff,
            count(logs.staff_id) as regstaff,
            count(staff.id) - count(logs.staff_id) as unregstaff
            '))
            ->where('staff.reg_id', $reg_id)
            ->where('staff.status', 'Active')
            ->groupBy('deps.id','deps.devision', 'deps.department')
            ->orderBy('deps.devision')
            ->orderBy('deps.department')
            ->get();

        return view('logs.show_dep', compact('reg', 'summary'));
    }

    public function showAll($reg_id)
    {
        $this->middleware('isAdmin');
        $reg = Reg::findOrFail($reg_id);

        $summary = DB::table('staff')
            ->leftjoin('regs', 'regs.id', '=', 'staff.reg_id')
            ->leftjoin('logs', function ($join) {
                $join->on('staff.id', '=', 'logs.staff_id');
                $join->on('staff.reg_id', '=', 'logs.reg_id');
            })
            ->select(DB::raw('
            regs.id,
            regs.name,
            count(staff.id) as allstaff,
            count(logs.staff_id) as regstaff,
            count(staff.id) - count(logs.staff_id) as unregstaff
            '))
            ->where('staff.reg_id', $reg_id)
            ->where('staff.status', 'Active')
            ->groupBy('regs.id', 'regs.name')
            ->get();

        return view('logs.show_all', compact('reg', 'summary'));
    }

    public function showAns($reg_id){
        
        $this->middleware('isAdmin');        

        $reg = Reg::findOrFail($reg_id);

        $summary = DB::table('staff')
            ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
            ->leftjoin('logs', function ($join) {
                $join->on('staff.id', '=', 'logs.staff_id');
                $join->on('staff.reg_id', '=', 'logs.reg_id');
            })
            ->select(DB::raw('
            deps.devision,
            count(staff.id) as allstaff,
            count(logs.staff_id) as regstaff,
            count(staff.id) - count(logs.staff_id) as unregstaff
            '))
            ->where('staff.reg_id',$reg_id)
            ->where('staff.status', 'Active')
            ->groupBy('deps.devision')
            ->orderBy('deps.devision')
            ->get();

        return view('logs.show_ans', compact('reg', 'summary1', 'summary2'));
    }

    public function listunregistered($reg_id, Request $request)
    {
        $this->middleware('isAdmin');
        $reg = Reg::findOrFail($reg_id);

        $depList = DB::table('staff')
            ->join('deps', 'deps.id', '=', 'staff.dep_id')
            ->select('deps.devision')
            ->where('staff.reg_id', $reg_id)
            ->where('staff.status', 'Active')
            ->groupBy('deps.devision')
            ->get();

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $staffUnRegist = DB::table('staff')
                ->leftjoin('logs', function ($join) {
                    $join->on('staff.id', '=', 'logs.staff_id');
                    $join->on('staff.reg_id', '=', 'logs.reg_id');
                })
                ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
                ->select('staff.id', 'staff.code', 'staff.name', 'deps.devision', 'deps.department')
                ->where('logs.id')
                ->where('staff.reg_id', $reg_id)
                ->where('staff.status', 'Active')
                ->where(function ($q) use ($keyword) {
                    $q->where('staff.code', 'like', '%' . $keyword . '%')
                        ->orWhere('staff.name', 'like', '%' . $keyword . '%');
                })
                ->orderBy('staff.code')
                ->paginate($perPage);
        } else {
            $staffUnRegist = DB::table('staff')
                ->leftjoin('logs', function ($join) {
                    $join->on('staff.id', '=', 'logs.staff_id');
                    $join->on('staff.reg_id', '=', 'logs.reg_id');
                })
                ->leftjoin('deps', 'deps.id', '=', 'staff.dep_id')
                ->select('staff.id', 'staff.code', 'staff.name', 'deps.devision', 'deps.department')
                ->where('logs.id')
                ->where('staff.reg_id', $reg_id)
                ->where('staff.status', 'Active')
                ->orderBy('staff.code')
                ->paginate($perPage);
        }

        return view('logs.listunregistered', compact('depList', 'reg', 'staffUnRegist'));
    }

    // public function qr_code($reg_id)
    // {
    //     return QrCode::size(300)->generate(url('/logs/listDiv/'.$reg_id));
    // }
}
