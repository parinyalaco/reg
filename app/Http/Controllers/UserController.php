<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $regs = User::where(function ($q) use ($keyword) {
                $q->Where('name', 'like', '%' . $keyword . '%')
                ->orWhere('username', 'like', '%' . $keyword . '%')
                ->orWhere('email', 'like', '%' . $keyword . '%');
            })->latest()->paginate($perPage);
            // $regs = User::latest()->paginate($perPage);
        } else {
            $regs = User::latest()->paginate($perPage);
        }

        return view('user.index', compact('regs'));
    }

    public function create()
    {
        return view('user.create');
    }

    // public function store(Request $request)
    // {
        
    //     $requestData = $request->all();        

    //     Staff::create($requestData);

    //     return redirect('staffs')->with('flash_message', ' added!');
    // }

    public function edit($id)
    {
        $reg = User::findOrFail($id);

        return view('user.edit', compact('reg'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'=>'required|max:255',
            'username'=>'required|max:255',
            'email'=>'required|email',          
        ],
        [
            'name.required'=>"กรุณาระบุชื่อพนักงานด้วยค่ะ",
            'name.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",
            'username.required'=>"กรุณาระบุชื่อเข้าใช้ระบบของพนักงานด้วยค่ะ",
            'username.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",
            'email.required'=>"กรุณาระบุ email ด้วยค่ะ",
            'email.email'=>"ข้อมูลไม่เป็นรูปแบบ email",
        ]);

        if(!empty($request->password)){
            $this->validate($request, [
                'password' => 'required|same:password_confirmation|min:4',
                'password_confirmation' => 'required',         
            ],
            [
                'password.required'=>"กรุณาระบุรหัสผ่านใหม่",
                'confirmPassword.required'=>"กรุณายืนยันรหัสผ่านใหม่",
            ]);
        }
        $requestData = $request->all();
        // dd($requestData['name']);
        $user_update['name'] = $requestData['name'];
        $user_update['email'] = $requestData['email'];
        $user_update['username'] = $requestData['username'];        
        if(!empty($user_update['password'])){
            $user_update['password'] = bcrypt($requestData['password']); 
        }

        $reg = User::findOrFail($id);
        $reg->update($user_update);

        return redirect('user')->with('flash_message', ' updated!');
    }

    public function destroy($id)
    {
        User::destroy($id);

        return redirect('user')->with('flash_message', ' deleted!');
    }
}
