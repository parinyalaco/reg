<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reg extends Model
{
    protected $fillable = [
        'name', 'desc', 'start_reg', 'end_reg', 'active','set_start','show'
    ];

    public function staff()
    {
        return $this->hasMany('App\Staff', 'reg_id')->where('status','Active');
    }
    
}
