<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Reg;
use Carbon\Carbon;

class autoClose extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Reg:AutoClose';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use to close Register';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = carbon::now();
        // echo $now;
        $check_reg = Reg::where('end_reg','<=',$now)->where('active','Active')->select('id');
        $reg_work['active'] = 'Disable';
        $reg_work['set_start'] = 0;
        $check_reg->update($reg_work);
    }
}
