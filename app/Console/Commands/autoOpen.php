<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Reg;
use Carbon\Carbon;

class autoOpen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Reg:AutoOpen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use to open Register';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = carbon::now();
        // echo $now;
        $start = Carbon::parse($now)->format('Y-m-d');
        $s_time = Carbon::parse($now)->format('H:i:s');
        // echo $start."  ";
        $check_reg = Reg::where('start_reg','<=',$now)->where('end_reg','>',$now)->where('active','Disable');
        // $query_check = $check_reg->toSql();        
        // // print_r($check_period);
        // echo "start : ".$start.", s_time : ".$s_time.", query : ".$query_check."</br>";
        // dd($query_check);
        $count_creg = $check_reg->count();
        // echo $count_creg."</br>";
        if($count_creg==1){
            $close_reg = Reg::where('active','Active')->count();
            // dd($close_reg);
            if($close_reg>0){
                // echo "; close_period : ".$check_period."; ";
                $reg_active['active'] = 'Disable';
                $reg_active['set_start'] = 0;
                // dd($period_active);
                $close_pr = Reg::where('active','Active')->update($reg_active);                
            }
            
            $value_check = $check_reg->select('id')->get();
            $reg_work['active'] = 'Active';
            $open_pr = $check_reg->update($reg_work);
            if($open_pr){
                return "Ok";
            }
        }
    }
}
