<?php

use App\Reg;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'LogsController@listDiv');
Route::get('/', function () 
{
    // return view('auth.login');  

    $reg = Reg::where('active', 'Active')->get();
    if(count($reg)==1){
        $qr_code['qr'] = 1;
        $qr_code['id'] = $reg[0]['id'];
    }else{
        $qr_code['qr'] = 0;
        $qr_code['id'] = '';
    }
    return view('auth.login',compact('qr_code')); 
     
});
Route::post('admin_login/login', 'Auth\LoginController@login')->name('admin_login.login');
Route::get('show_QR/{reg_id}', 'Auth\LoginController@show_QR')->name('show_QR');
Route::resource('admin_login', 'Auth\LoginController');

Route::resource('user', 'UserController');

Route::get('regs/change_active/{status}/{id}', 'RegsController@change_active')->name('change_active');
Route::get('regs/set_start', 'RegsController@set_start')->name('set_start');
Route::get('regs/upload/{reg_id}', 'RegsController@upload');
Route::post('regs/imports/{reg_id}', 'RegsController@uploadAction');
Route::get('regs/export/{reg_id}', 'RegsController@export');
Route::get('regs/clear_reg/{reg_id}', 'RegsController@clear_reg');
Route::resource('regs', 'RegsController');
Route::resource('deps', 'DepsController');
Route::get('staffs/index/{id}', 'StaffsController@index')->name('staffs_index');
Route::resource('staffs', 'StaffsController');

Route::get('imports/upload/{reg_id}', 'StaffsController@upload');
Route::post('imports/uploadAction/{reg_id}', 'StaffsController@uploadAction');

Route::get('logs/listDiv/{reg_id}', 'LogsController@listDiv');
Route::get('logs/listDep/{reg_id}/{devision}', 'LogsController@listDep');
Route::get('logs/liststaff/{reg_id}/{dep_id}', 'LogsController@liststaff');
Route::get('logs/register/{reg_id}/{staff_id}/{search}', 'LogsController@register');
Route::get('logs/unregister/{reg_id}/{id}', 'LogsController@unregister');
Route::get('logs/listregistered/{reg_id}', 'LogsController@listregistered');
Route::get('logs/listunregistered/{reg_id}', 'LogsController@listunregistered');
Route::get('logs/listregisteredtxt/{reg_id}', 'LogsController@listregisteredtxt');


Route::get('logs/showDiv/{reg_id}', 'LogsController@showDiv');
Route::get('logs/showDep/{reg_id}', 'LogsController@showDep');
Route::get('logs/showAll/{reg_id}', 'LogsController@showAll');
// Route::get('logs/showans/{reg_id}', 'LogsController@showAns');

Route::get('quizs/add/{id}', 'QuizsController@add');
Route::post('quizs/addAction/{id}', 'QuizsController@addAction');
Route::get('quizs/success/{check_txt}', 'QuizsController@success');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('qr-code', function () 
// {
//   return QRCode::text('QR Code Generator for Laravel!')->png();    
// });
// Route::get('qr_code/{reg_id}', function () {
//     return QrCode::size(300)->generate(url('/logs/listDiv/7'));
//     // return QrCode::size(300)->generate('A basic example of QR code!');
// });
Route::get('qr_code/{reg_id}', 'LogsController@qr_code')->name('qr_code');
Route::get('qrcode', function () 
{
    $reg = Reg::where('active', 'Active')->get();
    // dd($reg[0]['id']);
    if(count($reg)==1){
        $qr_code['qr'] = 1;
        $qr_code['id'] = $reg[0]['id'];
    }else{
        $qr_code['qr'] = 0;
        $qr_code['id'] = '';
    }
    // \QrCode::size(300)
    // ->format('png')
    // ->generate(url('/logs/listDiv/'.$qr_code['id']), public_path('images/qrcode.png'));

    return view('QRCode',compact('qr_code'));   
});